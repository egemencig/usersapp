//
//  ViewController.swift
//  UsersApp
//
//  Created by Egemen Çığ on 4.04.2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        API.sharedManager.getUsers { (response) in
            print(response)
        } errorHandler: { (error) in
            print(error)
        }

    }


}

