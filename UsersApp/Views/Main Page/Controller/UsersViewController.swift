//
//  UsersViewController.swift
//  UsersApp
//
//  Created by Egemen Çığ on 4.04.2021.
//

import UIKit
import SDWebImage

class UsersViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let searchController = UISearchController(searchResultsController: nil)
    var usersArray: [UsersResponse] = []
    var searchResults: [UsersResponse] = []
    var userIdArray: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestStatus = .pending
        setLayouts()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getUsers()
        searchController.searchBar.text = ""
        searchController.isActive = false
    }
    
    func setLayouts() {
        getUsers()
        setupTableView()
        setSearchBar()
        setupNavigationBar()
    }
    
    func setupNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = UIColor.darkGray
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.navigationController?.setStatusBar(backgroundColor: UIColor.darkGray)
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.largeTitleDisplayMode = .always
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        definesPresentationContext = true
        self.navigationItem.title = "Users"
        self.navigationItem.searchController = searchController
    }
    
    func setSearchBar() {
        searchController.searchBar.searchTextField.attributedPlaceholder = NSAttributedString(string: "Search...", attributes: [NSAttributedString.Key.foregroundColor : UIColor(red: 175/255, green: 175/255, blue: 175/255, alpha: 1.0), NSAttributedString.Key.font : UIFont(name: "HelveticaNeue-Medium", size: 13.0)!])
        searchController.searchBar.searchTextField.textColor = .black
        searchController.searchBar.searchTextField.font = UIFont(name: "HelveticaNeue-Medium", size: 15.0)
        searchController.dimsBackgroundDuringPresentation = false
        let attributes:[NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor.black,
            .font: UIFont(name: "HelveticaNeue-Bold", size: 18.0) ?? UIFont.systemFont(ofSize: 18.0)
        ]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(attributes, for: .normal)
        searchController.searchBar.delegate = self
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: "UserTableViewCell", bundle: nil), forCellReuseIdentifier: "userCell")
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func getUsers() {
        API.sharedManager.getUsers { [self] (response) in
            userIdArray = StoreManager.shared.getUserIdArray()
            self.usersArray = response
            for userId in userIdArray {
                if searchController.isActive == true && searchController.searchBar.text != "" {
                    if let index = searchResults.firstIndex(where: {$0.id == Int(userId)}) {
                        usersArray[index].status = true
                    }
                }
                else {
                    if let index = usersArray.firstIndex(where: {$0.id == Int(userId)}) {
                        usersArray[index].status = true
                    }
                }
                if userId == userIdArray.last {
                    self.tableView.reloadData()
                    self.requestStatus = .completed(nil)
                }
            }
            self.tableView.reloadData()
            self.requestStatus = .completed(nil)
        } errorHandler: { (error) in
            print("error")
        }
    }
    
    func showUserDetail(indexPath: IndexPath) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if let viewController = mainStoryboard.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController {
            if searchController.isActive == true && searchController.searchBar.text != "" {
                if let url = searchResults[indexPath.row].url, let userId = searchResults[indexPath.row].id {
                    viewController.url = url
                    viewController.userId = String(userId)
                }
            }
            else {
                if let url = usersArray[indexPath.row].url,let userId = usersArray[indexPath.row].id {
                    viewController.url = url
                    viewController.userId = String(userId)
                }
            }
            viewController.modalPresentationStyle = .fullScreen
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
}

extension UsersViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.showUserDetail(indexPath: indexPath)
    }
}

extension UsersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive == true && searchController.searchBar.text != "" {
            return searchResults.count
        }
        else {
            return usersArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath) as! UserTableViewCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        
        if searchController.isActive == true && searchController.searchBar.text != "" {
            if searchResults[indexPath.row].status == true {
                cell.statusImage.isHidden = false
            }
            
            else {
                cell.statusImage.isHidden = true
            }
            
            if let url = searchResults[indexPath.row].avatar_url, let imageUrl = URL(string: url) {
                cell.userImage.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "placeholder"), options: SDWebImageOptions(), context: nil)
            }
            
            if let userNick = searchResults[indexPath.row].login {
                cell.userNickname.text = userNick
            }
        }
        else {
            if usersArray[indexPath.row].status == true {
                cell.statusImage.isHidden = false
            }
            else {
                cell.statusImage.isHidden = true
            }
            
            if let url = usersArray[indexPath.row].avatar_url, let imageUrl = URL(string: url) {
                cell.userImage.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "placeholder"), options: SDWebImageOptions(), context: nil)
            }
            
            if let userNick = usersArray[indexPath.row].login {
                cell.userNickname.text = userNick
            }
        }
        
        return cell
    }
    
}

extension UsersViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchResults = usersArray.filter({ (UsersResponse) -> Bool in
            let match = UsersResponse.login?.range(of: searchText, options: .caseInsensitive)
            return (match != nil)
        })
        tableView.reloadData()
    }
    
}
