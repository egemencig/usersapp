//
//  UserTableViewCell.swift
//  UsersApp
//
//  Created by Egemen Çığ on 4.04.2021.
//

import UIKit

class UserTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNickname: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var colorView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        colorView.layer.cornerRadius = 16
        colorView.backgroundColor = UIColor.customGrayColor()
        userImage.contentMode = .scaleAspectFill
        userImage.layer.cornerRadius = userImage.frame.height / 2
        statusImage.layer.cornerRadius = statusImage.frame.height / 2
        statusImage.isHidden = true
    }
    
}
