//
//  DetailTableViewCell.swift
//  UsersApp
//
//  Created by Egemen Çığ on 4.04.2021.
//

import UIKit

class DetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var twitterLbl: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userNickLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var blogLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var twitterImg: UIImageView!
    @IBOutlet weak var locationImg: UIImageView!


    override func awakeFromNib() {
        super.awakeFromNib()
        avatarImg.layer.cornerRadius = avatarImg.frame.height / 2
        avatarImg.contentMode = .scaleAspectFill
        
        colorView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 20)
        
    }

    
}
