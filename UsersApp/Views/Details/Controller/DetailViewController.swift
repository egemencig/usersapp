//
//  DetailViewController.swift
//  UsersApp
//
//  Created by Egemen Çığ on 4.04.2021.
//

import UIKit
import SDWebImage

class DetailViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var userModel: UserModel = UserModel()
    var url: String = String()
    var userId: String = String()
    var userIdArray: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.customGrayColor()
        storeUserId()
        requestStatus = .pending
        setLayouts()
    }
    
    func storeUserId() {
        userIdArray = StoreManager.shared.getUserIdArray()
        if !userIdArray.contains(userId) {
            userIdArray.append(userId)
            StoreManager.shared.saveUserIdArray(data: userIdArray)
        }
    }
    
    func setLayouts() {
        getUserDetails()
        setupTableView()
    }
    
    func getUserDetails() {
        API.sharedManager.getUserDetails(endpoint: self.url) { (response) in
            self.userModel = response
            self.tableView.reloadData()
            self.requestStatus = .completed(nil)
            
        } errorHandler: { (error) in
            print("error")
        }
        
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: "DetailTableViewCell", bundle: nil), forCellReuseIdentifier: "detailCell")
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    @objc func cancel() {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension DetailViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell", for: indexPath) as! DetailTableViewCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        
        if let url = userModel.avatar_url, let imageUrl = URL(string: url) {
            cell.avatarImg.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "placeholder"), options: SDWebImageOptions(), context: nil)
        }
        
        if let userNick = userModel.login, let userName = userModel.name {
            cell.userNickLabel.text = userNick
            cell.userNameLabel.text = userName
        }
        
        if let followers = userModel.followers, let following = userModel.following {
            cell.followersLabel.text = String(followers)
            cell.followingLabel.text = String(following)
        }
        
        if let location = userModel.location {
            cell.locationLabel.text = location
        }
        
        if let blog = userModel.blog {
            cell.blogLabel.text = blog
        }
        
        if let company = userModel.company {
            cell.companyLabel.text = company
        }
        
        if userModel.location == nil {
            cell.locationImg.isHidden = true
        }
        
        else {
            cell.locationImg.isHidden = false
        }
        
        if userModel.twitter_username == nil {
            cell.twitterImg.isHidden = true
        }
        
        else {
            cell.twitterImg.isHidden = false
        }
        
        if let twitter = userModel.twitter_username {
            cell.twitterLbl.text = "@\(twitter)"
        }
        
        cell.cancelButton.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        
        return cell
        
    }
}
