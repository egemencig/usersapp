//
//  UserModel.swift
//  UsersApp
//
//  Created by Egemen Çığ on 4.04.2021.
//

import Foundation

class UsersResponse: Decodable {
    
    var login : String?
    var id : Int?
    var node_id : String?
    var avatar_url : String?
    var gravatar_id : String?
    var url : String?
    var html_url : String?
    var status: Bool?
}

