//
//  UIColorExtension.swift
//  UsersApp
//
//  Created by Egemen Çığ on 4.04.2021.
//


import Foundation
import UIKit

extension UIColor {
    
    class func customGrayColor() -> UIColor {
        return UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1.0)
    }
    
}


