//
//  StoreManager.swift
//  UsersApp
//
//  Created by Egemen Çığ on 4.04.2021.
//

import Foundation
import UIKit

class StoreManager {
    
    static let shared = StoreManager()

    private static func storeUserId(data : [String]) -> NSData {

        return NSKeyedArchiver.archivedData(withRootObject: data as NSArray) as NSData
    }
    
   
    func loadUserId() -> [String]? {

       if let unarchivedObject = UserDefaults.standard.object(forKey: "userIdArray") as? Data {

           return NSKeyedUnarchiver.unarchiveObject(with: unarchivedObject as Data) as? [String]
       }

       return nil
   }
    
    func getUserIdArray() -> [String] {
        var userIdArray: [String] = [String]()
        if StoreManager.shared.loadUserId() == nil {
            StoreManager.shared.saveUserIdArray(data: userIdArray)
        }
        else {
            userIdArray = StoreManager.shared.loadUserId() ?? []
        }
        return userIdArray
    }
    
    func saveUserIdArray(data : [String]?) {
       
       let archivedObject = StoreManager.storeUserId(data: data!)
       UserDefaults.standard.set(archivedObject, forKey: "userIdArray")
       UserDefaults.standard.synchronize()
   }
    
}
