//
//  API.swift
//  UsersApp
//
//  Created by Egemen Çığ on 4.04.2021.
//

import Foundation
import Alamofire

class API {
    
    static let sharedManager = API()
    private var sessionManager = SessionManager()
    private init() { }
    
    
    fileprivate let encoding = JSONEncoding.default
    
    func getUsers(sucees:@escaping ((_ status: [UsersResponse])-> Void), errorHandler:@escaping ((_ status: Bool)-> Void)){
        
        let endpoint = "https://api.github.com/users"
        sessionManager.request(endpoint, method: .get,encoding: JSONEncoding.default).responseData { (response) in
            let result = response.result
            switch result {
            case .success(let data):
                do {
                    let responseArray = try JSONDecoder().decode([UsersResponse].self, from: data)
                    sucees(responseArray)
                } catch  {
                    errorHandler(true)
                }
            case .failure(_ ):
                errorHandler(true)
            }
        }
    }
    
    
    func getUserDetails(endpoint: String,sucees:@escaping ((_ status: UserModel)-> Void), errorHandler:@escaping ((_ status: Bool)-> Void)){
        
        sessionManager.request(endpoint, method: .get,encoding: JSONEncoding.default).responseData { (response) in
            let result = response.result
            switch result {
            case .success(let data):
                do {
                    let responseArray = try JSONDecoder().decode(UserModel.self, from: data)
                    sucees(responseArray)
                } catch  {
                    errorHandler(true)
                }
            case .failure(_ ):
                errorHandler(true)
            }
        }
    }
    
}

